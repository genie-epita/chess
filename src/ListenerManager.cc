//
// Created by jj on 6/2/19.
//

#include "ListenerManager.hh"
#include "Piece.hh"
#include <set>

namespace board {

    using namespace listener;


    ListenerManager::ListenerManager()
        : listeners_(std::set<Listener*>())
        , listening_(true)
        {}

    ListenerManager::ListenerManager(bool listening)
            : listeners_()
            , listening_(listening)
    {}

    void ListenerManager::register_board(const ChessboardInterface &board_interface) {
        for (Listener* listener : listeners_)
            listener->register_board(board_interface);
    }

    void ListenerManager::on_game_finished() {
        for (Listener* listener : listeners_)
            listener->on_game_finished();
    }

    void ListenerManager::on_piece_moved(const PieceType piece, const Position& from, const Position &to) {
        for (Listener* listener : listeners_)
            listener->on_piece_moved(piece, from, to);
    }

    void ListenerManager::on_piece_taken(const PieceType piece, const Position &at) {
        for (Listener* listener : listeners_)
            listener->on_piece_taken(piece, at);
    }

    void ListenerManager::on_piece_promoted(const PieceType piece, const Position &at) {
        for (Listener* listener : listeners_)
            listener->on_piece_promoted(piece, at);
    }

    void ListenerManager::on_kingside_castling(const Color color) {
        for (Listener* listener : listeners_)
            listener->on_kingside_castling(color);
    }

    void ListenerManager::on_queenside_castling(const Color color) {
        for (Listener* listener : listeners_)
            listener->on_queenside_castling(color);
    }

    void ListenerManager::on_player_check(const Color color) {
        for (Listener* listener : listeners_)
            listener->on_player_check(color);
    }

    void ListenerManager::on_player_mat(const Color color) {
        for (Listener* listener : listeners_)
            listener->on_player_mat(color);
    }

    void ListenerManager::on_player_pat(const Color color) {
        for (Listener* listener : listeners_)
            listener->on_player_pat(color);
    }

    void ListenerManager::on_player_disqualified(const Color color) {
        for (Listener* listener : listeners_)
            listener->on_player_disqualified(color);
    }

    void ListenerManager::on_draw() {
        for (Listener* listener : listeners_)
            listener->on_draw();
    }

    bool ListenerManager::isListening() const {
        return listening_;
    }

    void ListenerManager::setListening(bool listening) {
        listening_ = listening;
    }

    const std::set<Listener *> &ListenerManager::getListeners() const {
        return listeners_;
    }

    void ListenerManager::registerListener(Listener *listener) {
        listeners_.insert(listener);
    }

    void ListenerManager::piece_taken(Piece* piece, const Position position)
    {
        on_piece_taken(piece->getPieceType(), position);
        if (piece->getPieceType() == PieceType::KING)
        {
            on_player_mat(piece->getColor());
            on_game_finished();
        }
    }

    void ListenerManager::notifyEnd(std::string& s, Color color) {
        if (s == "1-0"){
            on_player_mat(Color::BLACK);
            on_game_finished();
        }
        else if (s == "0-1") {
            on_player_mat(Color::WHITE);
            on_game_finished();
        }
        else if (s == "1/2-1/2"){
            on_draw();
        }
        else if (s == "*"){
            on_player_disqualified(color);
        }
    }

}
