//
// Created by jj on 5/25/19.
//

#include <iostream>
#include "piece-type.hh"

namespace board {
    std::ostream &operator<<(std::ostream &os, const PieceType &pieceType) {
        switch (pieceType) {
            case PieceType::QUEEN:
                return os << "Queen";
            case PieceType::ROOK:
                return os << "Rook";
            case PieceType::BISHOP:
                return os << "Bishop";
            case PieceType::KNIGHT:
                return os << "Knight";
            case PieceType::PAWN:
                return os << "Pawn";
            case PieceType::KING:
                return os << "King";
        }
        return os;
    }
}