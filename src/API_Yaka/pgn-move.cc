#include "pgn-move.hh"

namespace board
{
    PgnMove::PgnMove(const Position& start, const Position& end,
                     PieceType piece, bool capture, ReportType report,
                     const PgnMove::opt_piece_t& promotion)
        : start_(start)
        , end_(end)
        , piece_(piece)
        , promotion_(promotion)
        , capture_(capture)
        , castling_(false)
        , queenside_(false)
        , report_(report)
    {}

    //castling constructor
    PgnMove::PgnMove(const Position &start, const Position &end, PieceType piece, bool capture, ReportType report,
                     bool queenside, const PgnMove::opt_piece_t &promotion)
        : start_(start)
        , end_(end)
        , piece_(piece)
        , promotion_(promotion)
        , capture_(capture)
        , castling_(true)
        , queenside_(queenside)
        , report_(report)
    {}

    PgnMove PgnMove::generate_castling(bool queen_side, Color color)
    {
        static const Position wking_pos{File::E, Rank::ONE};
        static const Position bking_pos{File::E, Rank::EIGHT};
        static const PgnMove w_small{wking_pos,
                                     {File::G, Rank::ONE},
                                     PieceType::KING,
                                     false,
                                     ReportType::NONE,
                                     false};
        static const PgnMove w_big{wking_pos,
                                   {File::C, Rank::ONE},
                                   PieceType::KING,
                                   false,
                                   ReportType::NONE,
                                   true};
        static const PgnMove b_small{bking_pos,
                                     {File::G, Rank::EIGHT},
                                     PieceType::KING,
                                     false,
                                     ReportType::NONE,
                                     false};
        static const PgnMove b_big{bking_pos,
                                   {File::C, Rank::EIGHT},
                                   PieceType::KING,
                                   false,
                                   ReportType::NONE,
                                   false};

        if (color == Color::WHITE)
            return queen_side ? w_big : w_small;

        return queen_side ? b_big : b_small;
    }

    void PgnMove::report_set(ReportType report)
    {
        report_ = report;
    }

    std::ostream &operator<<(std::ostream &os, const PgnMove &move) {
        os << "Move(start=" << move.start_<< ", end=" << move.end_ << ", piece=" << move.piece_ << ", promotion=";
        if (move.promotion_)
            os << move.promotion_.value();
        else
            os << "Nope";
        os << ", capture=" << move.capture_ << ")";
        return os;
    }

    const Position &PgnMove::getStart() const {
        return start_;
    }

    const Position &PgnMove::getEnd() const {
        return end_;
    }

    PieceType PgnMove::getPiece() const {
        return piece_;
    }

    const PgnMove::opt_piece_t &PgnMove::getPromotion() const {
        return promotion_;
    }

    bool PgnMove::isCapture() const {
        return capture_;
    }

    bool PgnMove::isCastling() const {
        return castling_;
    }

    bool PgnMove::isQueenside() const {
        return queenside_;
    }

    ReportType PgnMove::getReport() const {
        return report_;
    }

} // namespace board
