//
// Created by jj on 5/25/19.
//
#include <iostream>
#include "position.hh"
#include "ChessBoard.hh"

namespace board
{
    const std::string Position::toString() const {
        std::string str;
        switch (file_get()) {
            case File::A:
                str = "a";
                break;
            case File::B:
                str = "b";
                break;
            case File::C:
                str = "c";
                break;
            case File::D:
                str = "d";
                break;
            case File::E:
                str = "e";
                break;
            case File::F:
                str = "f";
                break;
            case File::G:
                str = "g";
                break;
            case File::H:
                str = "h";
                break;
        }
        switch (rank_get()) {
            case Rank::ONE:
                str += "1";
                break;
            case Rank::TWO:
                str += "2";
                break;
            case Rank::THREE:
                str += "3";
                break;
            case Rank::FOUR:
                str += "4";
                break;
            case Rank::FIVE:
                str += "5";
                break;
            case Rank::SIX:
                str += "6";
                break;
            case Rank::SEVEN:
                str += "7";
                break;
            case Rank::EIGHT:
                str += "8";
                break;
        }

        return str;
    }

    std::ostream& operator<<(std::ostream& os, const Position& position) {
        return os << position.toString();
    }

    Tile* Position::getTile(ChessBoard& chessBoard) {
        int position = y_ * 10 + x_;
        return chessBoard.getBoard()[position];
    }


}