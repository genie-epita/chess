//
// Created by jj on 5/25/19.
//
#include <iostream>
#include "color.hh"

namespace board
{
    std::ostream& operator<<(std::ostream &os, const Color& color) {
        switch (color){
            case Color::WHITE:
                return os << "White";
            case Color::BLACK:
                return os << "Black";
        }
    }
}