
namespace board
{

    inline Position::Position(File file, Rank rank)
        : x_(static_cast<int>(file) + 1)
        , y_(static_cast<int>(rank) + 2)
    {}

    inline Position::Position(int x, int y)
            : x_(x)
            , y_(y)
    {}

    inline bool Position::operator==(const Position& pos) const
    {
        return x_ == pos.x_ && y_ == pos.y_;
    }

    inline bool Position::operator!=(const Position& pos) const
    {
        return x_ != pos.x_ || y_ != pos.y_;
    }

    inline File Position::file_get() const
    {
        return File(x_ - 1);
    }

    inline Rank Position::rank_get() const
    {
        return Rank(y_ - 2);
    }

    inline int Position::toInt() const {
        return x_ + 10 * y_;
    }

    inline Position& Position::setX(int x) {
        x_  = x;
        return *this;
    }

    inline Position& Position::setY(int y) {
        y_ = y;
        return *this;
    }

    inline Position& Position::setXY(int x, int y) {
        x_ = x;
        y_ = y;
        return *this;
    }

    inline Position& Position::addX(int x) {
        x_ += x;
        return *this;
    }

    inline Position& Position::addY(int y) {
        y_ += y;
        return *this;
    }

    inline Position& Position::addXY(int x, int y) {
        x_ += x;
        y_ += y;
        return *this;
    }

} // namespace board