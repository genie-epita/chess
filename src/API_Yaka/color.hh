#pragma once

#include <iostream>

namespace board
{
    /* The Color enum represent a side. */
    enum class Color : bool
    {
        WHITE = false,
        BLACK = true
    };

    std::ostream& operator<<(std::ostream &os, const Color& color);

} // namespace board
