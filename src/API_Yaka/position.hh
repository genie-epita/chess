#pragma once

#include <utility>
#include <iostream>
#include "utype.hh"

namespace board
{
    class ChessBoard;
    class Tile;
    /* The file enum represent the colomns
     * on the board */
    enum class File
    {
        A,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
    };

    /* The rank enum represent the lines
     * on the board */
    enum class Rank
    {
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE,
        SIX,
        SEVEN,
        EIGHT,
    };


    /* Position represent a coordinate on the board */
    class Position
    {
    public:
        Position(int x, int y);
        Position(File file, Rank rank);

        File file_get() const;
        Rank rank_get() const;

        Position& setX(int x);
        Position& setY(int y);
        Position& setXY(int x, int y);

        Position& addX(int x);
        Position& addY(int y);
        Position& addXY(int x, int y);

        Tile* getTile(ChessBoard& chessBoard);

        bool operator==(const Position& pos) const;
        bool operator!=(const Position& pos) const;

        int toInt() const;

        const std::string toString() const;
        friend std::ostream& operator<<(std::ostream &os, const Position &position);

    private:
        int x_;
        int y_;
    };

    } // namespace board

#include "position.hxx"
