//
// Created by jj on 5/28/19.
//

#include <sstream>
#include <iostream>
#include <fstream>
#include <random>
#include "uci.hh"
#include "AI.hh"
#include "position.hh"
#include "Tile.hh"

int AI::run() {
    ai::init("Pawno");

    AI ai = AI();
    board::GameManager gm;

    while (true) {
        std::string board = ai::get_board();
        if (board == "quit")
            break;

        std::stringstream line;
        line.str(board);

        std::string cmd;
        line >> cmd;

        std::string opt;
        line >> opt;
        if (opt == "startpos") {
            gm.loadDefaultBoard();
        } else {
            gm.loadBoard(line);
            std::string t1;
            std::string t2;

            line >> t1;
            line >> t2;
        }
        std::string m;
        if (line >> m)
        {
            while (line >> m) {

                auto start = board::Position((m[0] - 'a') + 1, (m[1] - '1') + 2);
                auto end = board::Position((m[2] - 'a') + 1, (m[3] - '1') + 2);
                auto move = gm.createMove(start, end, m.length() == 5 ? m[4] : '\0');
                gm.move(move);
            }
        }

        board::Move bestmove = ai.bestMove(gm);
        ai::play_move(bestmove.toString());

    }
    return 0;
}

AI::AI()
    : bestMoves_(std::vector<board::Move>())
{}

board::Move AI::bestMove(board::GameManager& game) {
    int max = INT32_MIN;

    auto& board = game.getBoard();

    std::vector<board::Move> moves = board.getPossibleMoves(game.getTurn());

    for (auto& move : moves) {
        game.move(move);
        int score = minimum(game, 3);
        if (score > max) {
            max = score;
            bestMoves_.clear();
            bestMoves_.push_back(move);
        }
        else if (score == max)
            bestMoves_.push_back(move);
        game.unMove();
    }

    if (max == INT32_MIN)
    {
        for (auto& move : moves) {
            game.move(move);
            int score = minimum(game, 1);
            if (score > max) {
                max = score;
                bestMoves_.clear();
                bestMoves_.push_back(move);
            }
            else if (score == max)
                bestMoves_.push_back(move);
            game.unMove();
        }
    }

    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<int> uni(0, bestMoves_.size() - 1);
    return bestMoves_.at(uni(rng));
}

int AI::evaluateBoard(board::ChessBoard& board, board::Color color) {
    int score = 0;
    bool foundK = false;
    bool foundKN = false;

    for (int i = 21; i < 101; i += 10) {
        for (int j = 0; j < 8; j++) {
            board::Tile* current = board.getBoard()[i + j];
            if (!current->isEmpty()) {
                board::Piece* piece = current->getPiece();
                if (piece->getColor() == color) {
                    score += piece->getValue();
                    if (piece->getPieceType() == board::PieceType::KING)
                        foundK = true;
                }
                else {
                    score -= piece->getValue();
                    if (piece->getPieceType() == board::PieceType::KING)
                        foundKN = true;
                }
            }
        }
    }

    if (foundK) {
        if (foundKN)
            return score;
        return INT32_MAX;
    }
    return INT32_MIN;
}

int AI::maximum(board::GameManager& game, int depth) {
    if (depth == 0)
        return evaluateBoard(game.getBoard(), game.getTurn());

    int max = INT32_MIN;
    int bestBoard = INT32_MIN;
    auto moves = game.getBoard().getPossibleMoves(game.getTurn());

    for (auto& move : moves) {
        game.move(move);
        int score = minimum(game, depth - 1);
        if (score > max) {
            max = score;
            bestBoard = -evaluateBoard(game.getBoard(), game.getTurn());
        }
        else if (score == max) {
            int tmp = -evaluateBoard(game.getBoard(), game.getTurn());
            if (tmp > bestBoard) {
                bestBoard = tmp;
            }
        }
        game.unMove();
    }

    return max;
}

int AI::minimum(board::GameManager& game, int depth) {
    if (depth == 0)
        return -evaluateBoard(game.getBoard(), game.getTurn());

    int min = INT32_MAX;
    int bestBoard = INT32_MAX;
    auto moves = game.getBoard().getPossibleMoves(game.getTurn());

    for (auto& move : moves) {
        game.move(move);
        int score = maximum(game, depth - 1);
        if (score < min) {
            min = score;
            bestBoard = evaluateBoard(game.getBoard(), game.getTurn());
        }
        else if (score == min) {
            int tmp = evaluateBoard(game.getBoard(), game.getTurn());
            if (tmp < bestBoard) {
                bestBoard = tmp;
            }
        }
        game.unMove();
    }

    return min;
}