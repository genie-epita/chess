//
// Created by jj on 5/28/19.
//

#ifndef CHESS_AI_HH
#define CHESS_AI_HH

#include "ChessBoard.hh"
#include "GameManager.hh"
#include "Logger.hh"

class AI {
public:
    static int run();

    AI();

    board::Move bestMove(board::GameManager& game);
    static int evaluateBoard(board::ChessBoard& board, board::Color color);
    int maximum(board::GameManager& game, int depth);
    int minimum(board::GameManager& game, int depth);

private:
    std::vector<board::Move> bestMoves_;
};


#endif //CHESS_AI_HH
