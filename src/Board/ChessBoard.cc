//
// Created by jj on 5/21/19.
//

#include "ChessBoard.hh"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>
#include "Piece.hh"
#include "Rook.hh"
#include "Queen.hh"
#include "King.hh"
#include "Bishop.hh"
#include "Pawn.hh"
#include "Knight.hh"
#include "Tile.hh"

namespace board
{
    ChessBoard::ChessBoard()
            : board_(std::array<Tile*, 120>())
            , enPassant_(nullptr)
            , turn_(Color::WHITE)
    {
        for (int i = 0; i < 120; i++)
            board_[i] = nullptr;

        for (int i = 0; i < 20; i++)
        {
            board_[i] = new Tile();
            board_[120 - i - 1] = new Tile();
        }

        for (int i = 20; i < 100; i+= 10)
        {
            board_[i] = new Tile();
            board_[i + 9] = new Tile();
        }

        std::stringstream stringstream;
        stringstream.str("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1 1");
        loadFen(stringstream);
    }

    ChessBoard::ChessBoard(std::istream& fen)
            : board_(std::array<Tile*, 120>())
            , enPassant_(nullptr)
            , turn_(Color::WHITE)
    {
        for (int i = 0; i < 120; i++)
            board_[i] = nullptr;

        for (int i = 0; i < 20; i++)
        {
            board_[i] = new Tile();
            board_[120 - i - 1] = new Tile();
        }
        for (int i = 20; i < 100; i+= 10)
        {
            board_[i] = new Tile();
            board_[i + 9] = new Tile();
        }

        loadFen(fen);
    }

    ChessBoard::~ChessBoard() {
        for (int i = 0; i < 120; i++)
            if (board_[i] != nullptr)
                delete(board_[i]);
    }

    Tile* ChessBoard::operator[](const Position &position) const {
        return board_[position.toInt()];
    }


    std::vector<Move> ChessBoard::getPossibleMoves(Color color) {
        auto moves = std::vector<Move>();

        for (Tile* tile : board_)
            if (tile->isOnBoard() && !tile->isEmpty() && color == tile->getPiece()->getColor())
                tile->getPiece()->possibleMoves(moves);


        getPossibleCastlings(moves, color);

        return moves;
    }



    void ChessBoard::getPossibleCastlings(std::vector<Move> &moves, Color color) {
        Rank rank = color == Color::WHITE ? Rank::ONE : Rank::EIGHT;

        std::vector<Tile*> tiles;
        bool is_calculated = false;

        Tile* k_tile = Position(File::E, rank).getTile(*this);
        if (!k_tile->isEmpty()
                && k_tile->getPiece()->getPieceType() == PieceType::KING
                && k_tile->getPiece()->getMoveCount() == 0
                )
        {
            Tile* rk_tile = Position(File::H, rank).getTile(*this);
            if (!rk_tile->isEmpty()
                    && rk_tile->getPiece()->getPieceType() == PieceType::ROOK
                    && rk_tile->getPiece()->getMoveCount() == 0
                    && Position(File::F, rank).getTile(*this)->isEmpty()
                    && Position(File::G, rank).getTile(*this)->isEmpty())
            {
                is_calculated = true;
                for (Tile* tile : board_)
                    if (tile->isOnBoard() && !tile->isEmpty() && color != tile->getPiece()->getColor())
                        tile->getPiece()->threatenedTiles(tiles);

                if (std::find(tiles.begin(), tiles.end(), Position(File::E, rank).getTile(*this)) == tiles.end()
                   && std::find(tiles.begin(), tiles.end(), Position(File::F, rank).getTile(*this)) == tiles.end()
                   && std::find(tiles.begin(), tiles.end(), Position(File::G, rank).getTile(*this)) == tiles.end())
                {
                    moves.emplace_back(k_tile->getPosition(), Position(File::G, rank), false);
                }
            }

            Tile* rq_tile = Position(File::A, rank).getTile(*this);
            if (!rq_tile->isEmpty()
                && rq_tile->getPiece()->getPieceType() == PieceType::ROOK
                && rq_tile->getPiece()->getMoveCount() == 0
                && Position(File::C, rank).getTile(*this)->isEmpty()
                && Position(File::D, rank).getTile(*this)->isEmpty())
            {
                if (!is_calculated)
                    for (Tile* tile : board_)
                        if (tile->isOnBoard() && !tile->isEmpty() && color != tile->getPiece()->getColor())
                            tile->getPiece()->threatenedTiles(tiles);

                if (std::find(tiles.begin(), tiles.end(), Position(File::C, rank).getTile(*this)) == tiles.end()
                    && std::find(tiles.begin(), tiles.end(), Position(File::D, rank).getTile(*this)) == tiles.end()
                    && std::find(tiles.begin(), tiles.end(), Position(File::E, rank).getTile(*this)) == tiles.end())
                {
                    moves.emplace_back(k_tile->getPosition(), Position(File::C, rank), true);
                }
            }
        }
    }

    const std::array<Tile*, 120>& ChessBoard::getBoard() const {
        return board_;
    }

    void ChessBoard::loadFen(std::istream &fen) {
        auto p = Position(File::A, Rank::EIGHT);

        char c = 0;
        while (fen.peek() == ' ')
            fen.get();
        while (!fen.eof())
        {
            c = fen.get();
            if (c == ' ')
                break;
            if (c == '/') {
                p.addY(-1);
                p.setX(1);
            } else {
                Tile* tile = new Tile(this, p, nullptr);
                board_[p.toInt()] = tile;
                switch (c) {
                    case 'K':
                        tile->setPiece(new King(Color::WHITE, tile));
                        break;
                    case 'k':
                        tile->setPiece(new King(Color::BLACK, tile));
                        break;
                    case 'Q':
                        tile->setPiece(new Queen(Color::WHITE, tile));
                        break;
                    case 'q':
                        tile->setPiece(new Queen(Color::BLACK, tile));
                        break;
                    case 'B':
                        tile->setPiece(new Bishop(Color::WHITE, tile));
                        break;
                    case 'b':
                        tile->setPiece(new Bishop(Color::BLACK, tile));
                        break;
                    case 'N':
                        tile->setPiece(new Knight(Color::WHITE, tile));
                        break;
                    case 'n':
                        tile->setPiece(new Knight(Color::BLACK, tile));
                        break;
                    case 'R':
                        tile->setPiece(new Rook(Color::WHITE, tile));
                        break;
                    case 'r':
                        tile->setPiece(new Rook(Color::BLACK, tile));
                        break;
                    case 'P':
                        tile->setPiece(new Pawn(Color::WHITE, tile));
                        break;
                    case 'p':
                        tile->setPiece(new Pawn(Color::BLACK, tile));
                        break;
                    default:
                        delete tile;
                        break;
                }
                if (c > '0' && c < '9') {
                    int n = c - '0';
                    for (int i = 0; i < n; i++) {
                        board_[p.toInt()] = new Tile(this, p, nullptr);
                        p.addX(1);
                    }
                } else
                    p.addX(1);
            }
        }

        std::string turn;
        fen >> turn;

        turn_ = turn == "w" || turn == "W" ? Color::WHITE : Color::BLACK;

        std::string castling;
        fen >> castling;

        if (castling.find('K') == std::string::npos)
        {
            Piece* rook = Position(File::H, Rank::ONE).getTile(*this)->getPiece();
            if (rook != nullptr)
                rook->setMoveCount(1);
        }
        else if (castling.find('Q') == std::string::npos)
        {
            Piece* rook = Position(File::A, Rank::ONE).getTile(*this)->getPiece();
            if (rook != nullptr)
                rook->setMoveCount(1);
        }
        else if (castling.find('k') == std::string::npos)
        {
            Piece* rook = Position(File::H, Rank::EIGHT).getTile(*this)->getPiece();
            if (rook != nullptr)
                rook->setMoveCount(1);
        }
        else if (castling.find('q') == std::string::npos)
        {
            Piece* rook = Position(File::A, Rank::EIGHT).getTile(*this)->getPiece();
            if (rook != nullptr)
                rook->setMoveCount(1);
        }

        std::string enPassant;
        fen >> enPassant;

        if (enPassant != "-")
            enPassant_ = Position(File(enPassant[0] - 'A'), static_cast<Rank>(enPassant[1] - '1')).getTile(*this);
    }

    constexpr std::array<char, 6> piece_char_white{ 'Q',  'R', 'B', 'N', 'P', 'K' };
    constexpr std::array<char, 6> piece_char_black{ 'q',  'r', 'b', 'n', 'p', 'k' };

    void ChessBoard::printPiece(board::Tile* tile, std::ostream& os) const {
        if (tile->getPiece()->getColor() == Color::WHITE)
        {
            PieceType pt = tile->getPiece()->getPieceType();
            os << piece_char_white[static_cast<int>(pt)];
        }
        else
        {
            PieceType pt = tile->getPiece()->getPieceType();
            os << piece_char_black[static_cast<int>(pt)];
        }
    }

    void ChessBoard::print(std::ostream& os) const {
        int width = 8;
        int height = 8;

        os << "\n  ";
        for (char c = 'A'; c < 'A' + width; c++)
            os << "  " << c << " ";
        os << "\n  ---------------------------------\n";
        for (int i = height - 1; i >= 0; i--)
        {
            os << i + 1 << " |";
            for (int j = 0; j < width; j++)
            {
                Position p = Position(File(j), Rank(i));
                os << " ";

                if (board_[p.toInt()]->isEmpty())
                    os << " ";
                else
                    printPiece(board_[p.toInt()], os);
                os << " |";
            }
            os << " " << i + 1 << "\n  ";
            os << "---------------------------------\n";
        }
        os << "  ";
        for (char c = 'A'; c < 'A' + width; c++)
            os << "  " << c << " ";
        os << "\n";
    }

    Tile *ChessBoard::getEnPassant() const {
        return enPassant_;
    }

    void ChessBoard::setEnPassant(Tile *enPassant) {
        enPassant_ = enPassant;
    }

    Color ChessBoard::getTurn() const {
        return turn_;
    }

}