//
// Created by jj on 5/21/19.
//

#ifndef CHESS_CHESSBOARD_HH
#define CHESS_CHESSBOARD_HH

#include <array>
#include <stack>
#include <vector>
#include "Move.hh"
#include "chessboard-interface.hh"

namespace board {

    class ChessBoard : public ChessboardInterface {
    public:
        ChessBoard();
        explicit ChessBoard(std::istream& istream);

        ~ChessBoard() override;

        Tile* operator[](const Position& position) const override;
        std::vector<Move> getPossibleMoves(Color color);

        const std::array<Tile*, 120>& getBoard() const;

        void print(std::ostream& os) const;

        Tile *getEnPassant() const;

        Color getTurn() const;

        void setEnPassant(Tile *enPassant);

    private:
        void printPiece(Tile* tile, std::ostream& os) const;
        void loadFen(std::istream& fen);
        void getPossibleCastlings(std::vector<Move>& moves, Color color);

    private:
        std::array<Tile*, 120> board_;
        Tile* enPassant_;
        Color turn_;
    };
}

#endif //CHESS_CHESSBOARD_HH
