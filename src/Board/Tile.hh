//
// Created by jj on 5/21/19.
//

#ifndef CHESS_TILE_HH
#define CHESS_TILE_HH

#include "Piece.hh"
#include <iostream>
#include "position.hh"

namespace board {
    class Tile {
    public:
        Tile();
        Tile(ChessBoard* chessBoard, const Position& position, Piece* piece);

        ~Tile();

        bool isOnBoard() const;

        ChessBoard& getChessBoard() const;

        const Position& getPosition() const;

        bool isEmpty() const;

        Piece* getPiece() const;

        void setPiece(Piece *piece);

    private:
        const bool isOnBoard_;
        ChessBoard* chessBoard_;
        Position* position_;
        Piece* piece_;
    };

    std::ostream& operator<<(std::ostream &os, const Tile& tile);
}

#endif //CHESS_TILE_HH
