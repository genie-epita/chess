//
// Created by jj on 5/21/19.
//

#include <exception>
#include <iomanip>
#include "Tile.hh"

namespace board {

    Tile::Tile()
            : isOnBoard_(false)
            , chessBoard_(nullptr)
            , position_(nullptr)
            , piece_(nullptr)
    {}

    Tile::Tile(ChessBoard* chessBoard, const Position& position, board::Piece *piece)
            : isOnBoard_(true)
            , chessBoard_(chessBoard)
            , position_(new Position(position))
            , piece_(piece)
    {}

    Tile::~Tile() {
        delete piece_;
        delete position_;
    }

    bool Tile::isOnBoard() const {
        return isOnBoard_;
    }

    ChessBoard& Tile::getChessBoard() const {
        if (chessBoard_ == nullptr)
            throw std::logic_error("You must not get the chessboard of a Tile which is outside of the board");
        return *chessBoard_;
    }

    const Position& Tile::getPosition() const {
        if (position_ == nullptr)
            throw std::logic_error("You must not get the position of a Tile which is outside of the board");
        return *position_;
    }

    bool Tile::isEmpty() const{
        return piece_ == nullptr;
    }

    Piece* board::Tile::getPiece() const {
        return piece_;
    }

    void board::Tile::setPiece(board::Piece *piece) {
        piece_ = piece;
    }

    std::ostream &operator<<(std::ostream &os, const Tile &tile) {
        if (tile.getPiece() == nullptr)
            os << "null";
        else {
            Piece &p = *tile.getPiece();
            os << p;
        }
        os << "(" << tile.getPosition() << ")";
        return os;
    }
}
