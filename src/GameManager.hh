//
// Created by jj on 5/22/19.
//

#ifndef CHESS_GAMEMANAGER_HH
#define CHESS_GAMEMANAGER_HH

#include "ChessBoard.hh"
#include <set>
#include <vector>
#include "pgn-move.hh"
#include "ListenerManager.hh"

namespace listener
{
    class Listener;
}
namespace board {

    class GameManager {
    public:
        GameManager();
        ~GameManager();

        // Consider that all moves given are correct
        void move(Move& m1);
        void unMove();

        void registerListener(listener::Listener* listener);

        void printAllPossibleMovesNumber(Color color);
        void printBoard(std::ostream& os);

        void makeMoves(std::vector<board::PgnMove>& vector);
        int perft(std::ifstream& ifstream);

        void loadBoard(std::istream& istream);
        void loadDefaultBoard();

        ListenerManager& getListenerManager();

        ChessBoard& getBoard();

        Color getTurn() const;

        Move createMove(Position start, Position end, char piece);

    private:
        Move getCastlingMove(Color color, bool queenside);
        void unMove(Move& m1);
        int perftRec(int depth);
        bool kingInDanger(Color color);

    private:
        ChessBoard* chessBoard_;
        ListenerManager listeners_;
        std::stack<Move> moveList_;

        Color turn_;
        std::string castling_;
        std::string passant_;
        int fifty_;
        int plays_;
        int depth_;

    };
}

#endif //CHESS_GAMEMANAGER_HH
