//
// Created by jj on 5/28/19.
//

#ifndef CHESS_PGN_HH
#define CHESS_PGN_HH

#include <iostream>
#include <fstream>
#include "listener.hh"
#include "pgn-parser.hh"

class Pgn {
public:
    static int run(std::string &file, std::vector<listener::Listener *>& listeners);
};


#endif //CHESS_PGN_HH
