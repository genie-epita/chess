//
// Created by jj on 5/28/19.
//

#include "Pgn.hh"
#include "GameManager.hh"

int Pgn::run(std::string &file, std::vector<listener::Listener *>& listeners) {
    std::ifstream ifstream = std::ifstream(file);
    if (!ifstream.is_open())
    {
        std::cerr << "chessengine: Can't open the file named: " << file << std::endl;
        return 1;
    }

    pgn_parser::parse_header(ifstream);
    auto vs = pgn_parser::parse_body(ifstream);
    auto fin = vs[vs.size() - 1];
    vs.pop_back();
    auto vbp = pgn_parser::string_to_move(vs);

    auto gameManager = board::GameManager();
    gameManager.loadDefaultBoard();

    for (listener::Listener* listener : listeners)
        gameManager.registerListener(listener);

    gameManager.makeMoves(vbp);
    gameManager.getListenerManager().notifyEnd(fin, gameManager.getTurn());

    return 0;
}
