#include <iostream>
#include <cstring>
#include <dlfcn.h>
#include "listener.hh"
#include "Options.hh"
#include "Perft.hh"
#include "Pgn.hh"
#include "AI.hh"
#include <boost/program_options/variables_map.hpp>


int main(int argc, char** argv) {

    auto vm = Options::parse(argc, argv);

    if (vm.count("perft"))
    {
        std::string file = vm["perft"].as<std::string>();
        return Perft::run(file);
    }

    std::vector<listener::Listener*> listeners;
    std::vector<void*> dls;

    if (vm.count("listeners")) {

        for (const std::string& string : vm["listeners"].as<std::vector<std::string>>()) {
            void* dl = dlopen(string.c_str(), RTLD_LAZY);
            listener::Listener* listener = reinterpret_cast<listener::Listener*(*)()>(dlsym(dl, "listener_create"))();
            listeners.push_back(listener);
            dls.push_back(dl);
        }
    }
    int r_value = 0;

    if (vm.count("pgn"))
    {
        std::string file = vm["pgn"].as<std::string>();
        r_value = Pgn::run(file, listeners);
    }
    else
        r_value = AI::run();

    for (void* dl : dls)
        dlclose(dl);

    return r_value;
}