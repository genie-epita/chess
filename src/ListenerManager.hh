//
// Created by jj on 6/2/19.
//

#ifndef CHESS_LISTENERMANAGER_HH
#define CHESS_LISTENERMANAGER_HH

#include <set>
#include "listener.hh"

namespace board {

    class Piece;

    class ListenerManager : listener::Listener {
    public:
        ListenerManager();
        explicit ListenerManager(bool listening);

        void register_board(const board::ChessboardInterface &board_interface) override;
        void on_game_finished() override;
        void on_piece_moved(board::PieceType piece, const board::Position &from, const board::Position &to) override;
        void on_piece_taken(board::PieceType piece, const board::Position &at) override;
        void on_piece_promoted(board::PieceType piece, const board::Position &at) override;
        void on_kingside_castling(board::Color color) override;
        void on_queenside_castling(board::Color color) override;
        void on_player_check(board::Color color) override;
        void on_player_mat(board::Color color) override;
        void on_player_pat(board::Color color) override;
        void on_player_disqualified(board::Color color) override;
        void on_draw() override;

        bool isListening() const;
        void setListening(bool listening);

        const std::set<Listener*>& getListeners() const;
        void registerListener(Listener* listeners);


        void piece_taken(Piece* piece, Position position);
        void notifyEnd(std::string& s, Color color);

    private:
        std::set<listener::Listener*> listeners_;
        bool listening_;
    };

}

#endif //CHESS_LISTENERMANAGER_HH
