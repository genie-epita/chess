//
// Created by jj on 5/22/19.
//

#ifndef CHESS_LOGGER_HH
#define CHESS_LOGGER_HH

#include <listener.hh>
#include <ostream>

namespace board {
    class Logger : public listener::Listener {
    public:
        Logger(std::ostream& ostream);

        void register_board(const board::ChessboardInterface &board_interface) override;

        void on_game_finished() override;

        void on_piece_moved(board::PieceType piece, const board::Position &from, const board::Position &to) override;

        void on_piece_taken(board::PieceType piece, const board::Position &at) override;

        void on_piece_promoted(board::PieceType piece, const board::Position &at) override;

        void on_kingside_castling(board::Color color) override;

        void on_queenside_castling(board::Color color) override;

        void on_player_check(board::Color color) override;

        void on_player_mat(board::Color color) override;

        void on_player_pat(board::Color color) override;

        void on_player_disqualified(board::Color color) override;

        void on_draw() override;

    private:
        std::ostream& ostream_;
    };
}

#endif //CHESS_LOGGER_HH
