//
// Created by jj on 5/22/19.
//

#include "Logger.hh"

board::Logger::Logger(std::ostream& ostream)
    : ostream_(ostream)
    {}

void board::Logger::register_board(const board::ChessboardInterface &board_interface) {
    ostream_ << "Registered " << &board_interface << std::endl;
}

void board::Logger::on_game_finished() {
    ostream_ << "On game finished\n";
}

void
board::Logger::on_piece_moved(const board::PieceType piece, const board::Position &from, const board::Position &to) {
    ostream_ << "On piece moved " << piece << " - " << from << " -> " << to << std::endl;
}

void board::Logger::on_piece_taken(const board::PieceType piece, const board::Position &at) {
    ostream_ << "On piece taken " << piece << " - " << at << std::endl;
}

void board::Logger::on_piece_promoted(const board::PieceType piece, const board::Position &at) {
    ostream_ << "On piece promoted " << piece << " - " << at << std::endl;
}

void board::Logger::on_kingside_castling(const board::Color color) {
    ostream_ << "On king side castling " << color << std::endl;
}

void board::Logger::on_queenside_castling(const board::Color color) {
    ostream_ << "On queen side castling " << color << std::endl;
}

void board::Logger::on_player_check(const board::Color color) {
    ostream_ << "On player chec " << color << std::endl;
}

void board::Logger::on_player_mat(const board::Color color) {
    ostream_ << "On player mat " << color << std::endl;
}

void board::Logger::on_player_pat(const board::Color color) {
    ostream_ << "On player pat " << color << std::endl;
}

void board::Logger::on_player_disqualified(const board::Color color) {
    ostream_ << "On player disqualified: " << color << std::endl;
}

void board::Logger::on_draw() {
    ostream_ << "On draw\n";
}