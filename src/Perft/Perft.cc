//
// Created by jj on 5/28/19.
//

#include "Perft.hh"

int Perft::run(std::string& file) {
    std::ifstream ifstream = std::ifstream(file);
    if (!ifstream.is_open())
    {
        std::cerr << "chessengine: Can't open the file named: " << file << std::endl;
        return 1;
    }

    auto gameManager = board::GameManager();
    gameManager.loadBoard(ifstream);
    int possibleMoveCount = gameManager.perft(ifstream);
    std::cout << possibleMoveCount;

    return 0;
}
