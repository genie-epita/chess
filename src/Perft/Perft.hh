//
// Created by jj on 5/28/19.
//

#ifndef CHESS_PERFT_HH
#define CHESS_PERFT_HH

#include <iostream>
#include <fstream>
#include "ChessBoard.hh"
#include "GameManager.hh"
#include "Logger.hh"
#include "Tile.hh"

class Perft {
public:
    static int run(std::string& file);
};


#endif //CHESS_PERFT_HH
