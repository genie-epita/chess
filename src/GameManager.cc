//
// Created by jj on 5/22/19.
//

#include <iostream>
#include <cstring>
#include <exception>
#include <fstream>
#include "pgn-move.hh"
#include "GameManager.hh"
#include "Tile.hh"
#include "Bishop.hh"
#include "Queen.hh"
#include "Rook.hh"
#include "Knight.hh"
#include "Pawn.hh"

namespace board
{

    GameManager::GameManager()
            : chessBoard_(nullptr)
            , listeners_(ListenerManager(true))
            , moveList_(std::stack<Move>())
            , turn_(Color::WHITE)
            , castling_()
            , passant_()
            , fifty_(0)
            , plays_(0)
            , depth_(0)
    {}

    GameManager::~GameManager() {
        delete chessBoard_;
    }

    void GameManager::loadBoard(std::istream& istream) {
        chessBoard_ = new ChessBoard(istream);
        turn_ = chessBoard_->getTurn();
    }

    void GameManager::loadDefaultBoard() {
        chessBoard_ = new ChessBoard();
        turn_ = chessBoard_->getTurn();
    }

    void GameManager::registerListener(listener::Listener *listener) {
        listeners_.registerListener(listener);
    }


    void GameManager::move(Move& m1) {
        moveList_.push(m1);

        Tile* tileStart = m1.getStartPosition().getTile(*chessBoard_);
        Tile* tileEnd = m1.getEndPosition().getTile(*chessBoard_);

        if (!m1.isCastling())
            listeners_.on_piece_moved(tileStart->getPiece()->getPieceType(), m1.getStartPosition(), m1.getEndPosition());


        if (m1.is2Pawn()) {
            if (tileStart->getPiece()->getColor() == Color::WHITE)
                chessBoard_->setEnPassant(Position(tileStart->getPosition()).addY(1).getTile(*chessBoard_));
            else
                chessBoard_->setEnPassant(Position(tileStart->getPosition()).addY(-1).getTile(*chessBoard_));
        }
        else
            chessBoard_->setEnPassant(nullptr);


        Piece* capture = tileStart->getPiece()->move(tileStart, tileEnd);

        if (m1.isEnPassant())
        {
            capture = m1.getCapture();
            capture->getTile()->setPiece(nullptr);
        }

        if (capture != nullptr) {
            listeners_.piece_taken(capture, m1.getEndPosition());
        }

        if (m1.isPromoted()) {
            tileEnd->setPiece(m1.getPromotion());
            m1.getPromotion()->setTile(tileEnd);
            listeners_.on_piece_promoted(m1.getPromotion()->getPieceType(), tileEnd->getPosition());
        }

        if (m1.isCastling())
        {
            if (m1.isQueenside())
                listeners_.on_kingside_castling(tileEnd->getPiece()->getColor());
            else
                listeners_.on_queenside_castling(tileEnd->getPiece()->getColor());

            Move m2 = getCastlingMove(tileEnd->getPiece()->getColor(), m1.isQueenside());
            move(m2);
            moveList_.pop();
            turn_ = turn_ == Color::WHITE ? Color::BLACK : Color::WHITE;
        }

        turn_ = turn_ == Color::WHITE ? Color::BLACK : Color::WHITE;
    }

    void GameManager::unMove(Move& m1)
    {
        Tile* tileStart = m1.getStartPosition().getTile(*chessBoard_);
        Tile* tileEnd = m1.getEndPosition().getTile(*chessBoard_);


        Piece* piece = tileEnd->getPiece();
        if (m1.isPromoted()) {
            piece = new Pawn(piece->getColor(), tileEnd);
        }
        else
            piece->unMove();

        tileStart->setPiece(piece);
        piece->setTile(tileStart);
        if (m1.isCapture())
        {
            if (m1.isEnPassant()) {
                m1.getCapture()->getTile()->setPiece(m1.getCapture());
                tileEnd->setPiece(nullptr);
            }
            else {
                tileEnd->setPiece(m1.getCapture());
                m1.getCapture()->setTile(tileEnd);
            }
        }
        else
            tileEnd->setPiece(nullptr);

        if (m1.isCastling())
        {
            Move m2 = getCastlingMove(tileStart->getPiece()->getColor(), m1.isQueenside());
            unMove(m2);
        }
    }

    void GameManager::unMove() {
        Move& move = moveList_.top();
        unMove(move);
        moveList_.pop();
        turn_ = turn_ == Color::WHITE ? Color::BLACK : Color::WHITE;
    }

    void GameManager::makeMoves(std::vector<PgnMove>& vector) {
        for (auto& m : vector)
        {
            Tile* startTile = Position(m.getStart()).getTile(*chessBoard_);
            Tile* endTile = Position(m.getEnd()).getTile(*chessBoard_);
            if (m.isCastling()) {
                Move my_move = Move(m.getStart(), m.getEnd(), m.isQueenside());
                move(my_move);
            }
            else
            {
                Piece *capture = (*chessBoard_)[m.getEnd()]->getPiece();
                Piece *promotion = nullptr;
                if (m.getPromotion()) {
                    auto p = m.getPromotion().value();
                    switch (p) {
                        case PieceType::BISHOP:
                            promotion = new Bishop(startTile->getPiece()->getColor(), endTile);
                            break;
                        case PieceType::QUEEN:
                            promotion = new Queen(startTile->getPiece()->getColor(), endTile);
                            break;
                        case PieceType::ROOK:
                            promotion = new Rook(startTile->getPiece()->getColor(), endTile);
                            break;
                        case PieceType::KNIGHT:
                            promotion = new Knight(startTile->getPiece()->getColor(), endTile);
                            break;
                        case PieceType::PAWN:
                            break;
                        case PieceType::KING:
                            break;
                    }
                }
                Move my_move = Move(m.getStart(), m.getEnd(), capture, promotion);
                move(my_move);
                if (kingInDanger(getTurn()))
                {
                    listeners_.on_player_check(getTurn());
                }
                if (kingInDanger(getTurn() == Color::WHITE ? Color::BLACK : Color::WHITE))
                {
                    listeners_.on_player_check(getTurn());
                }
            }
        }
    }

    bool GameManager::kingInDanger(Color color)
    {
        std::vector<Tile*> tiles;
        Tile* kingTile = nullptr;

        for (Tile* tile : chessBoard_->getBoard()) {
            if (tile->isOnBoard() && !tile->isEmpty())
            {
                Piece* piece = tile->getPiece();
                if(color != piece->getColor())
                    tile->getPiece()->threatenedTiles(tiles);
                else if (piece->getPieceType() == PieceType::KING)
                    kingTile = tile;
            }
        }

        for (auto tile : tiles)
        {
            if (tile == kingTile)
                return true;
        }

        return kingTile == nullptr;
    }

    int GameManager::perftRec(int depth) {
        if (depth == 0)
            return 1;

        auto pm = chessBoard_->getPossibleMoves(getTurn());
        int sum = 0;
        for (auto m : pm)
        {
            move(m);
            if (!kingInDanger(getTurn() == Color::WHITE ? Color::BLACK : Color::WHITE))
                sum += perftRec(depth - 1);
            unMove();
        }

        return sum;
    }

    int GameManager::perft(std::ifstream& perft) {
        perft >> fifty_ >> plays_ >> depth_;
        return perftRec(depth_);
    }

    void GameManager::printAllPossibleMovesNumber(Color color) {
        auto pm = chessBoard_->getPossibleMoves(color);
        for (Move& move : pm)
        {
            auto* piece = move.getStartPosition().getTile(*chessBoard_)->getPiece();
            std::cout << move.getStartPosition() << "->" << move.getEndPosition();
            std::cout << ": " << piece->getColor() << " " << piece->getPieceType() << std::endl;
        }
    }

    void GameManager::printBoard(std::ostream& os) {
        chessBoard_->print(os);
    }

    ListenerManager& GameManager::getListenerManager() {
        return listeners_;
    }

    ChessBoard& GameManager::getBoard() {
        return *chessBoard_;
    }


    Move GameManager::getCastlingMove(Color color, bool queenside) {
        if (color == Color::WHITE) {
            if (queenside)
                return Move(Position(File::A, Rank::ONE), Position(File::D, Rank::ONE));
            else
                return Move(Position(File::H, Rank::ONE), Position(File::F, Rank::ONE));
        }
        else {
            if (queenside)
                return Move(Position(File::A, Rank::EIGHT), Position(File::D, Rank::EIGHT));
            else
                return Move(Position(File::H, Rank::EIGHT), Position(File::F, Rank::EIGHT));
        }
    }

    Move GameManager::createMove(Position start, Position end, char piece) {
        auto moves = chessBoard_->getPossibleMoves(start.getTile(*chessBoard_)->getPiece()->getColor());

        for (auto& move : moves) {
            if (move.isPromoted()) {
                if (move.getStartPosition() != start || move.getEndPosition() != end)
                    continue;
                switch (piece) {
                    case 'q':
                        if (move.getPromotion()->getPieceType() == PieceType::QUEEN)
                            return move;
                        break;
                    case 'r':
                        if (move.getPromotion()->getPieceType() == PieceType::ROOK)
                            return move;
                        break;
                    case 'k':
                        if (move.getPromotion()->getPieceType() == PieceType::KNIGHT)
                            return move;
                        break;
                    case 'b':
                        if (move.getPromotion()->getPieceType() == PieceType::BISHOP)
                            return move;
                        break;
                    default:
                        break;
                }
            }
            else {
                if (move.getStartPosition() == start && move.getEndPosition() == end)
                    return move;
            }
        }

        throw std::logic_error("Impossible, you should not be here");
    }

    Color GameManager::getTurn() const {
        return turn_;
    }
}