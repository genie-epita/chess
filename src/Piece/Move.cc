//
// Created by jj on 5/21/19.
//

#include "Move.hh"
#include "Piece.hh"
#include "Tile.hh"

namespace board
{

    Position& Move::getStartPosition(){
        return startPosition_;
    }

    Position& Move::getEndPosition(){
        return endPosition_;
    }

    Move::Move(Position startPosition, Position endPosition)
            : startPosition_(startPosition)
            , endPosition_(endPosition)
            , capture_(nullptr)
            , promotion_(nullptr)
            , castling_(false)
            , queenside_(false)
            , enPassant_(false)
            , is2Pawn_(false)
    {}

    Move::Move(Position startPosition, Position endPosition, Piece* capture)
            : startPosition_(startPosition)
            , endPosition_(endPosition)
            , capture_(capture)
            , promotion_(nullptr)
            , castling_(false)
            , queenside_(false)
            , enPassant_(false)
            , is2Pawn_(false)
    {}

    Move::Move(Position startPosition, Position endPosition, bool queenside)
            : startPosition_(startPosition)
            , endPosition_(endPosition)
            , capture_(nullptr)
            , promotion_(nullptr)
            , castling_(true)
            , queenside_(queenside)
            , enPassant_(false)
            , is2Pawn_(false)
    {}

    Move::Move(Position startPosition, Position endPosition, Piece *capture, bool enPassant)
            : startPosition_(startPosition)
            , endPosition_(endPosition)
            , capture_(capture)
            , promotion_(nullptr)
            , castling_(false)
            , queenside_(false)
            , enPassant_(enPassant)
            , is2Pawn_(false)
    {}

    Move::Move(Position startPosition, Position endPosition, Piece* capture, Piece* promotion)
            : startPosition_(startPosition)
            , endPosition_(endPosition)
            , capture_(capture)
            , promotion_(promotion)
            , castling_(false)
            , queenside_(false)
            , enPassant_(false)
            , is2Pawn_(false)
    {}

    Move::Move(Position startPosition, Position endPosition, Piece* capture, Piece* promotion, bool is2Pawn)
            : startPosition_(startPosition)
            , endPosition_(endPosition)
            , capture_(capture)
            , promotion_(promotion)
            , castling_(false)
            , queenside_(false)
            , enPassant_(false)
            , is2Pawn_(is2Pawn)
    {}

    bool board::Move::isCapture() const {
        return capture_ != nullptr;
    }

    bool Move::isPromoted() const {
        return promotion_ != nullptr;
    }

    Piece* board::Move::getCapture() {
        return capture_;
    }

    Piece* board::Move::getPromotion() {
        return promotion_;
    }

    bool Move::isCastling() const {
        return castling_;
    }

    bool Move::isQueenside() const {
        return queenside_;
    }

    const std::string Move::toString() {
        std::string mv1 = startPosition_.toString();
        std::string mv2 = endPosition_.toString();

        if (isPromoted())
            return mv1 + mv2 + getPromotion()->toString();
        return mv1 + mv2;
    }

    std::ostream& operator<<(std::ostream& os, Move move) {
        return os << move.toString();
    }

    bool Move::isEnPassant() const {
        return enPassant_;
    }

    bool Move::is2Pawn() const {
        return is2Pawn_;
    }

}