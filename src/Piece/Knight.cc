//
// Created by jj on 5/21/19.
//

#include "Knight.hh"
#include "Tile.hh"
#include <iostream>
#include "Move.hh"

namespace board
{
    Knight::Knight(Color color, Tile* tile)
        : Piece(color, PieceType::KNIGHT, tile, 350)
    {}

    void Knight::possibleMoves(std::vector<Move> &moves) {
        Position actualPosition = tile_->getPosition();
        ChessBoard& chessBoard = tile_->getChessBoard();
        int x, y;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    x = i == 0 ? 2 : 1;
                    y = i == 0 ? 1 : 2;

                    if (j == 0)
                        x *= -1;
                    if (k == 0)
                        y *= -1;

                    Position nextPosition = actualPosition;
                    Tile *tile = nextPosition.addXY(x, y).getTile(chessBoard);
                    if (tile->isOnBoard()) {
                        if (tile->isEmpty())
                            moves.emplace_back(actualPosition, tile->getPosition());
                        else if (tile->getPiece()->getColor() != tile_->getPiece()->getColor())
                            moves.emplace_back(actualPosition, tile->getPosition(), tile->getPiece());
                    }
                }
            }
        }
    }

    void Knight::threatenedTiles(std::vector<Tile *> &tiles) {
        Position actualPosition = tile_->getPosition();
        ChessBoard& chessBoard = tile_->getChessBoard();
        int x, y;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    x = i == 0 ? 2 : 1;
                    y = i == 0 ? 1 : 2;

                    if (j == 0)
                        x *= -1;
                    if (k == 0)
                        y *= -1;

                    Position nextPosition = actualPosition;
                    Tile *tile = nextPosition.addXY(x, y).getTile(chessBoard);
                    if (tile->isOnBoard())
                        tiles.push_back(tile);
                }
            }
        }
    }

    const std::string Knight::toString() {
        return "k";
    }
}
