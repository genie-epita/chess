//
// Created by jj on 5/21/19.
//

#ifndef CHESS_QUEEN_HH
#define CHESS_QUEEN_HH

#include "Piece.hh"

namespace board {
    class Queen : public Piece {
    public:
        Queen(Color color, Tile* tile);
        ~Queen() override = default;

        void possibleMoves(std::vector<Move> &moves) override;

        void threatenedTiles(std::vector<Tile *> &tiles) override;

        const std::string toString();
    };
}
#endif //CHESS_QUEEN_HH
