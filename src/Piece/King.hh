//
// Created by jj on 5/21/19.
//

#ifndef CHESS_KING_HH
#define CHESS_KING_HH

#include "Piece.hh"

namespace board {
    class King : public Piece {
    public:
        King(Color color, Tile* tile);
        ~King() override = default;

        void possibleMoves(std::vector<Move> &moves) override;

        void threatenedTiles(std::vector<Tile *> &tiles) override;

        const std::string toString();
    };
}

#endif //CHESS_KING_HH
