//
// Created by jj on 5/21/19.
//

#ifndef CHESS_BISHOP_HH
#define CHESS_BISHOP_HH

#include "Piece.hh"
#include <vector>

namespace board {
    class Bishop : public Piece {
    public:
        Bishop(Color color, Tile* tile);
        ~Bishop() override = default;

        void possibleMoves(std::vector<Move>& moves) override;

        void threatenedTiles(std::vector<Tile*>& tiles) override;

        const std::string toString();
    };
}

#endif //CHESS_BISHOP_HH
