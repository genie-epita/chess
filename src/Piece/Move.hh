//
// Created by jj on 5/21/19.
//

#ifndef CHESS_MOVE_HH
#define CHESS_MOVE_HH

#include "position.hh"

namespace board {
    class Piece;

    class Move {
    public:
        Move(Position startPosition, Position endPosition);
        Move(Position startPosition, Position endPosition, Piece* capture);
        Move(Position startPosition, Position endPosition, bool queenside);
        Move(Position startPosition, Position endPosition, Piece* capture, bool enPassant);
        Move(Position startPosition, Position endPosition, Piece* capture, Piece* promotion);
        Move(Position startPosition, Position endPosition, Piece* capture, Piece* promotion, bool is2Pawn);

        Position& getStartPosition();
        Position& getEndPosition();

        bool isCapture() const;
        bool isPromoted() const;

        bool isCastling() const;
        bool isQueenside() const;
        bool isEnPassant() const;
        bool is2Pawn() const;

        Piece* getCapture();
        Piece* getPromotion();

        const std::string toString();
        friend std::ostream& operator<<(std::ostream &os, Move move);

    private:
        Position startPosition_;
        Position endPosition_;
        Piece* capture_;
        Piece* promotion_;
        bool castling_;
        bool queenside_;
        bool enPassant_;
        bool is2Pawn_;
    };

}


#endif //CHESS_MOVE_HH
