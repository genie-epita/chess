//
// Created by jj on 5/21/19.
//

#include "King.hh"
#include "Tile.hh"
#include "Move.hh"

#include <iostream>

namespace board
{
    King::King(Color color, Tile* tile)
        : Piece(color, PieceType::KING, tile, INT32_MAX / 2)
    {}

    void King::possibleMoves(std::vector<Move> &moves) {
        Position actualPosition = tile_->getPosition();
        ChessBoard& chessBoard = tile_->getChessBoard();
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (i == 0 && j == 0)
                    continue;

                Position nextPosition = actualPosition;
                Tile *tile = nextPosition.addXY(i, j).getTile(chessBoard);
                if (tile->isOnBoard()) {
                    if (tile->isEmpty())
                        moves.emplace_back(actualPosition, tile->getPosition());
                    else if (tile->getPiece()->getColor() != tile_->getPiece()->getColor())
                        moves.emplace_back(actualPosition, tile->getPosition(), tile->getPiece());
                }
            }
        }
    }

    void King::threatenedTiles(std::vector<Tile*> &tiles) {
        Position actualPosition = tile_->getPosition();
        ChessBoard& chessBoard = tile_->getChessBoard();
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (i == 0 && j == 0)
                    continue;

                Position nextPosition = actualPosition;
                Tile *tile = nextPosition.addXY(i, j).getTile(chessBoard);
                if (tile->isOnBoard())
                    tiles.push_back(tile);
            }
        }
    }

    const std::string King::toString() {
        return "k";
    }
}
