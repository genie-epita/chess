//
// Created by jj on 5/21/19.
//

#ifndef CHESS_KNIGHT_HH
#define CHESS_KNIGHT_HH

#include "Piece.hh"

namespace board {
    class Knight : public Piece {
    public:
        Knight(Color color, Tile* tile);
        ~Knight() override = default;

        void possibleMoves(std::vector<Move> &moves) override;

        void threatenedTiles(std::vector<Tile *> &tiles) override;

        const std::string toString();
    };
}


#endif //CHESS_KNIGHT_HH
