//
// Created by jj on 5/21/19.
//

#ifndef CHESS_PAWN_HH
#define CHESS_PAWN_HH

#include "Piece.hh"

namespace board {
    class Pawn : public Piece {
    public:
        Pawn(Color color, Tile* tile);
        ~Pawn() override = default;

        void possibleMoves(std::vector<Move> &moves) override;

        void threatenedTiles(std::vector<Tile *> &tiles) override;

        bool isFirstMove(Rank rank) const;
        bool isPromotion(Rank rank) const;

        const std::string toString();
    };
}

#endif //CHESS_PAWN_HH
