//
// Created by jj on 5/21/19.
//

#include <iomanip>
#include "Piece.hh"
#include "Tile.hh"

namespace board {

    Piece::Piece(Color color, PieceType pieceType, Tile* tile, int value)
        : color_(color)
        , pieceType_(pieceType)
        , tile_(tile)
        , value_(value)
    {}

    const board::Color &board::Piece::getColor() const {
        return color_;
    }

    const board::PieceType &board::Piece::getPieceType() const {
        return pieceType_;
    }

    const Position& Piece::getPosition() const {
        return tile_->getPosition();
    }

    std::ostream &operator<<(std::ostream &os, const Piece &piece) {
        os << piece.getColor() << " " << std::setw(6) << piece.getPieceType();
        return os;
    }

    void Piece::setTile(Tile *tile) {
        tile_ = tile;
    }

    int Piece::getMoveCount() const {
        return move_count_;
    }

    void Piece::setMoveCount(int count) {
        move_count_ = count;
    }

    Tile *Piece::getTile() const {
        return tile_;
    }

    Piece* Piece::move(Tile* startTile, Tile* tileEnd) {
        Piece* capture = tileEnd->getPiece();

        tileEnd->setPiece(startTile->getPiece());
        startTile->getPiece()->setTile(tileEnd);
        startTile->setPiece(nullptr);

        move_count_++;

        return capture;
    }

    void Piece::unMove() {
        move_count_--;
    }

    int Piece::getValue() {
        return value_;
    }

}