//
// Created by jj on 5/21/19.
//

#ifndef CHESS_PIECE_HH
#define CHESS_PIECE_HH

#include "color.hh"
#include "piece-type.hh"
#include <vector>

namespace board {

    class Tile;
    class Move;
    class Position;

    class Piece {
    protected:
        Piece(Color color, PieceType pieceType, Tile* tile, int value);
    public:
        virtual ~Piece() = default;

        virtual void possibleMoves(std::vector<Move>& moves) = 0;
        virtual void threatenedTiles(std::vector<Tile*>& tiles) = 0;
        virtual const std::string toString() = 0;

        const Color& getColor() const;

        const PieceType& getPieceType() const;

        const Position& getPosition() const;

        Tile *getTile() const;

        void setTile(Tile *tile);

        int getValue();

        friend std::ostream &operator<<(std::ostream &os, const Piece &piece);

        int getMoveCount() const;
        void setMoveCount(int count);

        Piece* move(Tile* startTile, Tile* endTile);
        void unMove();

    protected:
        Color color_;
        PieceType pieceType_;
        Tile* tile_;
        int value_;
        int move_count_ = 0;
    };

}


#endif //CHESS_PIECE_HH
