//
// Created by jj on 5/21/19.
//

#include "Tile.hh"
#include "Pawn.hh"
#include "Rook.hh"
#include "Bishop.hh"
#include "Queen.hh"
#include "Knight.hh"
#include "Move.hh"
#include "ChessBoard.hh"


namespace board
{
    Pawn::Pawn(Color color, Tile* tile)
        : Piece(color, PieceType::PAWN, tile, 100)
    {}

    bool Pawn::isFirstMove(Rank rank) const {
        return getColor() == Color::WHITE ? rank == Rank::TWO : rank == Rank::SEVEN;
    }

    bool Pawn::isPromotion(Rank rank) const {
        return getColor() == Color::WHITE ? rank == Rank::EIGHT : rank == Rank::ONE;
    }

    void Pawn::possibleMoves(std::vector<board::Move>& moves) {
        Position actualPosition = tile_->getPosition();
        ChessBoard& chessBoard = tile_->getChessBoard();

        int direction = getColor() == Color::WHITE ? 1 : -1;

        // 1 tile ahead
        Tile* tile1 = Position(actualPosition).addY(1 * direction).getTile(chessBoard);
        if (tile1->isOnBoard() && tile1->isEmpty())
        {
            if (isPromotion(tile1->getPosition().rank_get())) {
                moves.emplace_back(actualPosition, tile1->getPosition(), nullptr, new Rook(getColor(), tile1));
                moves.emplace_back(actualPosition, tile1->getPosition(), nullptr, new Bishop(getColor(), tile1));
                moves.emplace_back(actualPosition, tile1->getPosition(), nullptr, new Queen(getColor(), tile1));
                moves.emplace_back(actualPosition, tile1->getPosition(), nullptr, new Knight(getColor(), tile1));
            }
            else
                moves.emplace_back(actualPosition, tile1->getPosition());

            // 2 tiles ahead
            if (isFirstMove(actualPosition.rank_get()))
            {
                Tile *tile2 = Position(actualPosition).addY(2 * direction).getTile(chessBoard);
                if (tile2->isEmpty())
                    moves.emplace_back(actualPosition, tile2->getPosition(), nullptr, nullptr, true);
            }
        }

        // 1 tile left ahead
        Tile* tileL = Position(actualPosition).addX(1 * direction).addY(1 * direction).getTile(chessBoard);
        if (tileL->isOnBoard() && !tileL->isEmpty() && tileL->getPiece()->getColor() != getColor()) {
            if (isPromotion(tileL->getPosition().rank_get())) {
                moves.emplace_back(actualPosition, tileL->getPosition(), tileL->getPiece(), new Rook(getColor(), tileL));
                moves.emplace_back(actualPosition, tileL->getPosition(), tileL->getPiece(), new Bishop(getColor(), tileL));
                moves.emplace_back(actualPosition, tileL->getPosition(), tileL->getPiece(), new Queen(getColor(), tileL));
                moves.emplace_back(actualPosition, tileL->getPosition(), tileL->getPiece(), new Knight(getColor(), tileL));
            }
            else
                moves.emplace_back(actualPosition, tileL->getPosition(), tileL->getPiece());
        }

        // 1 tile right ahead
        Tile* tileR = Position(actualPosition).addX(-1 * direction).addY(1 * direction).getTile(chessBoard);
        if (tileR->isOnBoard() && !tileR->isEmpty() && tileR->getPiece()->getColor() != getColor()) {
            if (isPromotion(tileR->getPosition().rank_get())) {
                moves.emplace_back(actualPosition, tileR->getPosition(), tileR->getPiece(), new Rook(getColor(), tileR));
                moves.emplace_back(actualPosition, tileR->getPosition(), tileR->getPiece(), new Bishop(getColor(), tileR));
                moves.emplace_back(actualPosition, tileR->getPosition(), tileR->getPiece(), new Queen(getColor(), tileR));
                moves.emplace_back(actualPosition, tileR->getPosition(), tileR->getPiece(), new Knight(getColor(), tileR));
            }
            else
                moves.emplace_back(actualPosition, tileR->getPosition(), tileR->getPiece());
        }

        if (chessBoard.getEnPassant() != nullptr)
        {
            if (tileL == chessBoard.getEnPassant())
            {
                if (getColor() == Color::WHITE)
                {
                    Piece* capture = Position(chessBoard.getEnPassant()->getPosition()).addY(-1).getTile(chessBoard)->getPiece();
                    moves.emplace_back(actualPosition, tileL->getPosition(), capture, true);
                }
                else
                {
                    Piece* capture = Position(chessBoard.getEnPassant()->getPosition()).addY(1).getTile(chessBoard)->getPiece();
                    moves.emplace_back(actualPosition, tileL->getPosition(), capture, true);
                }
            }
            else if (tileR == chessBoard.getEnPassant())
            {
                if (getColor() == Color::WHITE)
                {
                    Piece* capture = Position(chessBoard.getEnPassant()->getPosition()).addY(-1).getTile(chessBoard)->getPiece();
                    moves.emplace_back(actualPosition, tileR->getPosition(), capture, true);
                }
                else
                {
                    Piece* capture = Position(chessBoard.getEnPassant()->getPosition()).addY(1).getTile(chessBoard)->getPiece();
                    moves.emplace_back(actualPosition, tileR->getPosition(), capture, true);
                }
            }
        }
    }

    void Pawn::threatenedTiles(std::vector<board::Tile*>& tiles) {
        Position actualPosition = tile_->getPosition();
        ChessBoard &chessBoard = tile_->getChessBoard();

        int direction = getColor() == Color::WHITE ? 1 : -1;

        // 1 tile left ahead
        Tile *tileL = Position(actualPosition).addX(1 * direction).addY(1 * direction).getTile(chessBoard);
        if (tileL->isOnBoard())
            tiles.push_back(tileL);

        // 1 tile right ahead
        Tile *tileR = Position(actualPosition).addX(-1 * direction).addY(1 * direction).getTile(chessBoard);
        if (tileR->isOnBoard())
            tiles.push_back(tileR);

        if (chessBoard.getEnPassant() != nullptr)
        {
            if (tileL == chessBoard.getEnPassant() || tileR == chessBoard.getEnPassant())
            {
                tiles.push_back(chessBoard.getEnPassant());
            }
        }
    }

    const std::string Pawn::toString() {
        return "p";
    }
}
