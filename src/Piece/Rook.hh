//
// Created by jj on 5/21/19.
//

#ifndef CHESS_ROOK_HH
#define CHESS_ROOK_HH

#include "Piece.hh"

namespace board {
    class Rook : public Piece {
    public:
        Rook(Color color, Tile* tile);
        ~Rook() override = default;

        void possibleMoves(std::vector<Move> &moves) override;

        void threatenedTiles(std::vector<Tile *> &tiles) override;

        const std::string toString();
    };
}

#endif //CHESS_ROOK_HH
