//
// Created by jj on 5/21/19.
//

#include "Queen.hh"
#include "Tile.hh"
#include "Move.hh"

namespace board
{
    Queen::Queen(Color color, Tile* tile)
        : Piece(color, PieceType::QUEEN, tile, 1000)
    {}

    void Queen::possibleMoves(std::vector<Move> &moves) {

        Position actualPosition = tile_->getPosition();
        ChessBoard& chessBoard = tile_->getChessBoard();

        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (i == 0 && j == 0)
                    continue;

                Position nextPosition = actualPosition;
                for (int k = 1; k < 15; ++k) {
                    Tile *tile = nextPosition.addXY(i, j).getTile(chessBoard);
                    if (tile->isOnBoard()) {
                        if (tile->isEmpty()) {
                            moves.emplace_back(actualPosition, tile->getPosition());
                            continue;
                        }
                        else if (tile->getPiece()->getColor() != tile_->getPiece()->getColor())
                            moves.emplace_back(actualPosition, tile->getPosition(), tile->getPiece());
                    }
                    break;
                }
            }
        }
    }

    void Queen::threatenedTiles(std::vector<Tile *> &tiles) {
        Position actualPosition = tile_->getPosition();
        ChessBoard& chessBoard = tile_->getChessBoard();

        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (i == 0 && j == 0)
                    continue;

                Position nextPosition = actualPosition;
                for (int k = 1; k < 15; ++k) {
                    Tile *tile = nextPosition.addXY(i, j).getTile(chessBoard);
                    if (tile->isOnBoard()) {
                        tiles.push_back(tile);
                        if (tile->isEmpty())
                            continue;
                    }
                    break;
                }
            }
        }
    }

    const std::string Queen::toString() {
        return "q";
    }
}
