//
// Created by jj on 5/28/19.
//

#ifndef CHESS_OPTIONS_HH
#define CHESS_OPTIONS_HH

#include <iostream>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


namespace po = boost::program_options;

class Options {

    public:
    static po::variables_map parse(int argc, char** argv);

};


#endif //CHESS_OPTIONS_HH
