//
// Created by jj on 5/28/19.
//

#include "Options.hh"

po::variables_map Options::parse(int argc, char **argv) {
    po::options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "show usage")
            ("pgn", po::value<std::string>(), "path to the PGN game file")
            ("listeners,l", po::value<std::vector<std::string>>()->multitoken(), "path to the PGN game file")
            ("perft", po::value<std::string>(), "path to a perft file")
            ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);


    if (vm.count("help")) {
        std::cout << desc << "\n";
        exit(0);
    }

    return vm;
}
