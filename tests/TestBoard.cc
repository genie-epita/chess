//
// Created by jj on 6/5/19.
//

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include "ChessBoard.hh"
#include "GameManager.hh"

using namespace board;

TEST(Board, TestSimpleMove) {
    // Part 1 : Test
    testing::internal::CaptureStdout();

    GameManager gm1;
    gm1.loadDefaultBoard();
    Move move1 = Move(Position(File::F, Rank::TWO), Position(File::F, Rank::FOUR));
    gm1.move(move1);

    gm1.printBoard(std::cout);

    std::string output = testing::internal::GetCapturedStdout();
    //Part 2 : Expected Output
    testing::internal::CaptureStdout();

    GameManager gm2;
    std::stringstream stringstream;
    stringstream.str("rnbqkbnr/pppppppp/8/8/5P2/8/PPPPP1PP/RNBQKBNR w KQkq -");
    gm2.loadBoard(stringstream);

    gm2.printBoard(std::cout);

    std::string outputExpected = testing::internal::GetCapturedStdout();
    // Test Equality
    ASSERT_EQ(output, outputExpected);
}

TEST(Board, UndoMove) {
    // Part 1 : Test
    testing::internal::CaptureStdout();

    GameManager gm1;
    gm1.loadDefaultBoard();
    Move move1 = Move(Position(File::F, Rank::TWO), Position(File::F, Rank::FOUR));
    gm1.move(move1);
    Move move2 = Move(Position(File::F, Rank::FOUR), Position(File::F, Rank::FIVE));
    gm1.move(move2);
    gm1.unMove();

    gm1.printBoard(std::cout);

    std::string output = testing::internal::GetCapturedStdout();
    //Part 2 : Expected Output
    testing::internal::CaptureStdout();

    GameManager gm2;
    std::stringstream stringstream;
    stringstream.str("rnbqkbnr/pppppppp/8/8/5P2/8/PPPPP1PP/RNBQKBNR w KQkq -");
    gm2.loadBoard(stringstream);

    gm2.printBoard(std::cout);

    std::string outputExpected = testing::internal::GetCapturedStdout();

    // Test Equality
    ASSERT_EQ(output, outputExpected);
}

TEST(Board, Undo2Move) {
    // Part 1 : Test
    testing::internal::CaptureStdout();

    GameManager gm1;
    gm1.loadDefaultBoard();
    Move move1 = Move(Position(File::F, Rank::TWO), Position(File::F, Rank::FOUR));
    gm1.move(move1);
    Move move2 = Move(Position(File::F, Rank::FOUR), Position(File::F, Rank::FIVE));
    gm1.move(move2);
    Move move3 = Move(Position(File::F, Rank::FIVE), Position(File::F, Rank::SIX));
    gm1.move(move3);
    gm1.unMove();
    gm1.unMove();

    gm1.printBoard(std::cout);

    std::string output = testing::internal::GetCapturedStdout();
    //Part 2 : Expected Output
    testing::internal::CaptureStdout();

    GameManager gm2;
    std::stringstream stringstream;
    stringstream.str("rnbqkbnr/pppppppp/8/8/5P2/8/PPPPP1PP/RNBQKBNR w KQkq -");
    gm2.loadBoard(stringstream);

    gm2.printBoard(std::cout);

    std::string outputExpected = testing::internal::GetCapturedStdout();

    // Test Equality
    ASSERT_EQ(output, outputExpected);
}