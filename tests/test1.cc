//
// Created by jj on 5/28/19.
//

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "Perft.hh"
#include <boost/process.hpp>
#include <iostream>

static int perft(std::string file)
{
    testing::internal::CaptureStdout();
    Perft::run(file);
    std::string output = testing::internal::GetCapturedStdout();
    return std::stoi(output);
}

namespace bp =::boost::process;

TEST(Perft, TestInitDepth1) {
    ASSERT_EQ(perft("../tests_files/perft/testDepth1.perft"), 20);
}

TEST(Perft, TestInitDepth2) {
    ASSERT_EQ(perft("../tests_files/perft/testDepth2.perft"), 400);
}

TEST(Perft, TestInitDepth3) {
    ASSERT_EQ(perft("../tests_files/perft/testDepth3.perft"), 8902);
}

TEST(Perft, TestInitDepth4) {
    ASSERT_EQ(perft("../tests_files/perft/testDepth4.perft"), 197281);
}

TEST(Perft, TestInitDepth5) {
    ASSERT_EQ(perft("../tests_files/perft/testDepth5.perft"), 4865609);
}

TEST(Perft, Test2)
{
    ASSERT_EQ(perft("../tests_files/perft/testRandom1.perft"), 22);
}

TEST(Perft, TestPromotionAndCastling)
{
    ASSERT_EQ(perft("../tests_files/perft/testRandom2.perft"), 44);
}